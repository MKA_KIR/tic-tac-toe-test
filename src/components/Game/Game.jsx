import React, {useState} from 'react';
import Board from "../Board";
import {calculateWinner} from "../../helper";

import './Game.scss';



const Game = () => {
    const [board, setBoard] = useState(Array(9).fill(null))
    const [xIsNext, setXIsNext] = useState(true)
    const winner = calculateWinner(board)
    console.log(winner)

    const handleClick = (idx) => {
        const boardCopy = [...board]
        if (winner || boardCopy[idx]) return
        boardCopy[idx] = xIsNext ? 'X' : 'O'
        setBoard(boardCopy)
        setXIsNext(!xIsNext)
    }
    return (
            <div className="wrapper">
                <Board squares={board} click={handleClick} />
        </div>
    );
};

export default Game;
