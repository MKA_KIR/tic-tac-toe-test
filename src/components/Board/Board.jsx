import React from 'react';

import './Board.scss';

import Square from "../Square";

const Board = ({squares, click}) => {
    return (
        <div className='container'>
            <div className="board">
                {
                    squares.map((square, idx) => (
                        <Square key={idx} value={square} onClick={() => click(idx)} />
                    ))
                }
            </div>
            <div className='score'>
                <p className="score-text">Score</p>
                <p className="score-text">Player:</p>
                <p className="score-text">Player 2:</p>
            </div>
        </div>
    );
};

export default Board;
