import React from 'react';
import './Square.scss';

const Square = (props) => {
    return (
        <a href='#' className="square brd" onClick={props.onClick}>{props.value}</a>
    );
};

export default Square;
